
PLACEHOLDER = '%s'

def update_sql(table, where, data, raw_data=None)
    if raw_data is None:
    	raw_data = {}
    	
    wheres = []
    fields = []
    values = []

    for field, value in data.iteritems():
        fields.append("%s=%s" % (field, PLACEHOLDER))
        values.append(value)

    for field, value in raw_data.iteritems():
        fields.append("%s=%s" % (field, value))

    for field, value in where.iteritems():
        fields.append("%s=%s" % (field, PLACEHOLDER))
        values.append(value)

    sql = "UPDATE %s SET %s WHERE %s" %
           (table, ",".join(fields), " AND ".join(wheres))

    return sql, values


def update(cursor, table, where, data, raw_data=None)

    # cursor.execute(*update_sql(table, where, data, raw_data))

    sql, value = update_sql(table, where, data, raw_data)

    cursor.execute(sql, values)


def insert_sql(table, data, raw_data)

    if raw_data is None:
    	raw_data = {}
    	
    fields = []
    holders = []
    values = []

    for field, value in data.iteritems()
        fields.append(field)
        holders.append(PLACEHOLDER)
        values.append(value)

    for field, value in raw_data.iteritems()
        fields.append(field)
        holders.append(value)

    sql = "INSERT INTO %s (%s) VALUES (%s)" %
           (table, ",".join(fields), " AND ".join(holders))

    return sql, values


def insert(cursor, table, data, raw_data)

    sql, values = insert_sql(table, data, raw_data)

    cursor.execute(sql, values)


def delete_sql(table, where)
    	
    wheres = []

    for field, value in where.iteritems()
        wheres.append("%s=%s" % (field,PLACEHOLDER))
        values.append(value)

    sql = "DELETE FROM %s WHERE %s" %
           (table, " AND ".join(wheres))

    return sql, values


def delete(cursor, table, where)
    	
    sql, values = delete_sql(table, data)

    cursor.execute(sql, values)





"""

account_table =Table(cursor, 'twitter_account')

account = account_table.get(id)

accunt_table.update({'id': 3}, {'status': 'done'})


"""

"""

class Table(object):
    def __init__(cursor, table, pk='id')
	self.cursor = cursor
	self.table = table
	self.pk = pk

    def get(pk=None, **kwargs):
        if pk:
           kwargs[self.pk] = pk
	
	wheres = []
	
	for field, value in kwargs.iteritems()
            wheres.append("%s=%%s" % field)
            values.append(value)
	
	sql = "SELECT * FROM %s WHERE %s" %
	   (self.table, " AND ".join(wheres))

    	self.cursor.execute(sql, values)
    	return self.cursor.fetchone()
	

    def update(where, data):
        return update(self.cursor, self.table, where, data)

    def insert(data):
        return insert(self.cursor, self.table, data)

    def deletet(pk=None, **kwargs):
        if pk:
           kwargs[self.pk] = pk
	
	wheres = []
	
	for field, value in kwargs.iteritems()
            wheres.append("%s=%%s" % field)
            values.append(value)
	
	sql = "DELETE FROM %s WHERE %s" %
	   (self.table, " AND ".join(wheres))

    	self.cursor.execute(sql, values)

"""

